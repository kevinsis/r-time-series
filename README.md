# projTS

## Overview

This is a package for time-series analysis. To install v1.0.4, simply do:

`install.packages("projTS_1.0.4.tar.gz", type="source", repos=NULL)`


**Features/modules**

1. Datasets: JB
2. Interactive Graphics: YL
3. Educational Vignette: JB
4. Generator for AR(p) and MA(q): SF
5. sample autocovariance: all
6. Durbin-Levinson algorithm: YL
7. Innovation algorithm: KS
8. periodogram: SF
9. functions for plotting: KS
10. Vignette: JB

---

## Admin

Authors:

* Kevin Siswandi
* Yiwen Lu
* Johannes Brokmeier
* Sadaf Fida Hussain

Reference book: [Brockwell et al., 2002](https://www.researchgate.net/profile/Paul_Louangrath/post/What_type_of_techniques_are_employed_for_regression_analysis_and_estimation_of_Non-Linear_Non-Stationary_data/attachment/59d61dda79197b807797af3a/AS%3A273712316649480%401442269510286/download/2002-Brockwell-Introduction+Time+Series+and+Forecasting.pdf)

---

## Helper Packages

Um einige Aktionen beim Erstellen von Paketen zu automatisieren, nutzen wir die Pakete usethis, devtools, roxygen2, testthat und knitr.


---
