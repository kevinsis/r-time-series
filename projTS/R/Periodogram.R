#' Estimate Spectral Density of a Time Series by a smoothed Periodogram
#'
#' A function/graph that displays information about the periodic components of a time series.
#'
#' @param x a time series vector
#' @export
#'
#' @examples
#' myPeriodogram(x <- rnorm(100))
#' spectrum(x <- rnorm(100), span = c(2,2), plot = FALSE)
myPeriodogram <- function(x) {
  #x <- as.list(x)
  n <- NROW(x)
  if (!is.null(dim(x))) {stop("X must a vector or time series")}

  dat <- as.data.frame(x = x)
  dat$FF <- abs(fft(dat$x)/sqrt(n))^2  # calculates the raw periodogram
  kern <- kernel("modified.daniell", c(1,1))        #smooth the periodogram by applying kernel
  spect <- kernapply(dat$FF, kern)
  dat <- as.data.frame(spect)
  P <- 4/n*dat$spect[1:ceiling(n/2)]  # only need the first (n/2)+1  values of FFT
  f <- (1:floor(n/2))/n # creats the harmonic frequencies
  return(list(spec = P,freq = f, mDaniell = kern))
}


