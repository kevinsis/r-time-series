#' projTS: A package for implementing time series analysis.
#'
#' The projTS package provides three important capabilities for time series
#' analysis: Autoregressive (AR), Moving Average (MA), as well as the
#' autocovariance function. In addition, algorithms for finding the best linear
#' predictors, such as the Durbin-Levinson and innovations algorithm are also
#' presented.
#'
#' @section projTS functions:
#' This package provides functions for time series analysis, including:
#' \enumerate{
#'   \item auto_cov
#'   \item ma_innovations
#'   \item sim_ar
#'   \item sim_ma
#'   \item quickPlot
#'   \item run_shiny
#'   \item durbin_levinson
#'   \item ma_simulate
#'}
#' Please refer to the individual function's help/man page for further details
#' on the usage and capabilities.
#'
#' @author
#' List of authors (not in any order):
#' \itemize{
#'   \item Kevin Siswandi
#'   \item Yiwen Lu
#'   \item Johannes Brokmeier
#'   \item Sadaf Fida Hussain
#' }
#'
#' @seealso
#' Project repository:
#' \url{https://bitbucket.org/kevinsis/r-time-series/}
#'
#' @docType package
#' @name projTS
NULL
