context("test-durbin_levinson")
library(projTS)

test_that("results from durbin levinson algorithm agree with stats::arima for order 1", {
  ar.sim <- arima.sim(n = 100, list(ar = 0.5))
  bench <- arima(ar.sim, order = c(1, 0, 0))
  expect_true(abs(durbin_levinson(ar.sim, 1) - bench$coef[1]) < 3 * sqrt(bench$sigma2))

  ar.sim2 <- arima.sim(n = 100, list(ar = 0.1))
  bench <- arima(ar.sim2, order = c(1, 0, 0))
  expect_true(abs(durbin_levinson(ar.sim2, 1) - bench$coef[1]) < 3 * sqrt(bench$sigma2))

  ar.sim3 <- arima.sim(n = 100, list(ar = -0.6))
  bench <- arima(ar.sim3, order = c(1, 0, 0))
  expect_true(abs(durbin_levinson(ar.sim3, 1) - bench$coef[1]) < 3 * sqrt(bench$sigma2))
})

test_that("results from durbin levinson algorithm agree with stats::arima for order 2", {
  ar.sim <- arima.sim(n = 100, list(ar = c(0.5, -0.5)))
  bench <- arima(ar.sim, order = c(2, 0, 0))
  ar_fit <- durbin_levinson(ar.sim, 2)
  expect_true(abs(ar_fit[1] - bench$coef[1]) < 3 * sqrt(bench$sigma2))
  expect_true(abs(ar_fit[2] - bench$coef[2]) < 3 * sqrt(bench$sigma2))

  ar.sim2 <- arima.sim(n = 100, list(ar = c(0.2, -0.8)))
  bench <- arima(ar.sim2, order = c(2, 0, 0))
  ar_fit2 <- durbin_levinson(ar.sim2, 2)
  expect_true(abs(ar_fit2[1] - bench$coef[1]) < 3 * sqrt(bench$sigma2))
  expect_true(abs(ar_fit2[2] - bench$coef[2]) < 3 * sqrt(bench$sigma2))

  ar.sim3 <- arima.sim(n = 100, list(ar = -c(0.3, 0.7)))
  bench <- arima(ar.sim3, order = c(2, 0, 0))
  ar_fit3 <- durbin_levinson(ar.sim3, 2)
  expect_true(abs(ar_fit3[1] - bench$coef[1]) < 3 * sqrt(bench$sigma2))
  expect_true(abs(ar_fit3[2] - bench$coef[2]) < 3 * sqrt(bench$sigma2))
})

test_that("ar_simulate gives (approximately) the same coefficient", {
  ar_boot1 <- ar_simulate(100, 100, 99, 0.5)
  expect_lt(abs(mean(ar_boot1$t) - 0.5), 3 * sd(ar_boot1$t))

  #ar_boot2 <- ar_simulate(100, 100, 99, 0.1)
  #expect_lt(abs(mean(ar_boot2$t) - 0.1), 3 * sd(ar_boot2$t))

  ar_boot3 <- ar_simulate(100, 100, 99, 0)
  expect_lt(abs(mean(ar_boot3$t) - 0), 3 * sd(ar_boot3$t))
})

test_that("ar_simulate encounters errors", {
  expect_error(ar_simulate(0, 100, 99, 0.5))
  expect_error(ar_simulate(2, 10, 99, 0.5))
  expect_error(ar_simulate(100, 100, -1, 0.5))
  expect_error(ar_simulate(100, 100, 99, NA))
})
